"""
Program do obsługi niewielkiej bazy szkolnej
złożonej z 4 typów użytkowników: nauczyciel, wychowawca, uczeń, klasa szkolna.
"""
import sys

school = {}  # słownik wszystkich osób i klas szkolnych


class SchoolClass:  # klasa dla typu użytkownika: klasa szkolna

    def __init__(self, name):
        self.name = name
        self.students = []
        self.teachers = []
        self.educator = []

    def display(self):
        print('Klasa {}'.format(self.name.upper()))
        if self.educator != []:
            print('Wychowawcą klasy {} jest {}.'.format(self.name.upper(), self.educator.name))
        print('Uczniowie klasy:')
        for student in self.students:
            print(student.name)


def make_class_list(user_name):  # funkcja pomocnicza do stworzernia listy klas dla nauczycieli i wychowawców
    class_list = []
    while True:
        class_name = input('Podaj nazwę klasy dla użytkownika {} lub kliknij Enter aby zakończyć:  '.format(
            user_name))
        if class_name == '':
            return class_list
        elif class_name not in school.keys():
            school[class_name] = SchoolClass(class_name)
        class_list.append(school[class_name])


class Educator:  # klasa dla typu użytkownika: Wychowawca

    def __init__(self, name):
        self.name = name
        self.classes = []

    def add_class(self):
        self.classes = make_class_list(self.name)
        for c in self.classes:
            c.educator = self

    def display(self):
        print('Wszyscy uczniowie wychowawcy {}:'.format(self.name))
        for cl in self.classes:
            for student in cl.students:
                print(student.name)


class Teacher:  # klasa dla typu użytkownika: Nauczyciel

    def __init__(self, name):
        self.name = name
        self.classes = []
        self.subject = ''

    def add_class(self):
        self.subject = input('Podaj nazwę przedmiotu, którego uczy nauczyciel {}  '.format(self.name))
        self.classes = make_class_list(self.name)
        for c in self.classes:
            c.teachers.append(self)

    def display(self):
        print('Wychowawcy wszystkich klas, z kórymi ma zajęcia nauczyciel {}:'.format(self.name))
        for cl in self.classes:
            if cl.educator != []:
                print('{} - {}'.format(cl.name.upper(), cl.educator.name))


class Student:  # klasa dla typu użytkownika: Uczeń

    def __init__(self, name):
        self.name = name
        self.student_class = ''

    def add_class(self):
        student_class = input('Podaj nazwę klasy dla użytkownika {}  '.format(self.name))
        if student_class not in school.keys():
            school[student_class] = SchoolClass(student_class)
        self.student_class = school[student_class]
        self.student_class.students.append(self)

    def display(self):
        print('Wszystkie lekcje ucznia {} i nauczyciele prowadzący:'.format(self.name))
        for teacher in self.student_class.teachers:
            print('{} - {}'.format(teacher.subject, teacher.name))


while True:
    user = input(
        """Podaj typ użytkownika, którego chcesz dodać do bazy:\n
'U' - uczeń\n'W' - wychowawca\n'N' - nauczyciel\n
Aby zakończyć wprowadzanie wybierz 'koniec'\n """
    )
    if user == 'koniec':
        break
    else:
        name = input('Podaj imię i nazwisko nowego użytkownika:  ')
        if user == 'U':
            person = Student(name)
        elif user == 'W':
            person = Educator(name)
        elif user == 'N':
            person = Teacher(name)
        else:
            print('Wprowadzono nieprawidłowy typ użytkownika - spróbuj ponownie')
            continue
    person.add_class()
    if name not in school.keys():
        school[name] = person
    else:
        school[name] = [school[name], person]

print('PODSUMOWANIE')
arg = sys.argv[1]

if arg not in school:
    print('Podany argument nie znajduję się w bazie.')
else:
    if type(school[arg]) == list:
        for element in school[arg]:
            element.display()
    else:
        element = school[arg]
        element.display()


