"""
Program służy do zwracania informacji o pogodzie (opadach) w danym dniu wskazanym przez użytkownika
 lub w dniu nastepnym (przy braku wskaznaia daty). Dane pogodowe są pobierane z wykorzystaniem zewnętrznego API:
 https://rapidapi.com/community/api/open-weather-map/
"""
import sys
import requests
import datetime
from datetime import datetime as dt
import os
import csv
import json

today = dt.combine(datetime.date.today(), dt.min.time())

if len(sys.argv) < 2:
    print('Nie podano wystarczającej ilości argumentów wejściowych. Brak klucza API.')
    sys.exit('Działanie programu zostanie przerwane')
elif len(sys.argv) == 2:
    date = datetime.timedelta(days=1) + today
    date = date.strftime('%Y-%m-%d')
else:
    date_str = sys.argv[2]
    date = dt.strptime(date_str, '%Y-%m-%d').strftime('%Y-%m-%d')

max_date = dt.combine(datetime.date.today(), dt.min.time()) + datetime.timedelta(days=15)
forecast_file = 'forecast.csv'
key = sys.argv[1]
city = "Warsaw,PL"
days_amount = 16
delta_time = dt.strptime(date, '%Y-%m-%d') - today


def import_current_forecast(city, days, key):
    url = "https://community-open-weather-map.p.rapidapi.com/forecast/daily"
    querystring = {"q": city, "cnt": days, "units": "metric or imperial", "lang": "en"}
    headers = {'x-rapidapi-key': key,
        'x-rapidapi-host': "community-open-weather-map.p.rapidapi.com"}

    return requests.request("GET", url, headers=headers, params=querystring)


def check_weather(forecast_file, date):
    if not os.path.exists(forecast_file):
        return False
    with open(forecast_file, 'r', newline='') as file:
        reader = csv.reader(file)
        for row in reader:
            name, forecast_str = row[0], row[1]
            if name == date:
                forecast_str = forecast_str.replace("\'", "\"")
                day_forecast = json.loads(forecast_str)
                if day_forecast['weather'][0]['main'] == 'Rain':
                    return 'Będzie padać'

                return 'Nie będzie padać.'


def update_forecast_and_check(forecast_list, forecast_file, date):
    with open(forecast_file, 'w', newline='') as file:
        fields = ['date', 'forecast_dict']
        dicts = []
        writer = csv.DictWriter(file, fields)
        for d in forecast_list:
            name = dt.combine(dt.utcfromtimestamp(d['dt']), dt.min.time()).strftime('%Y-%m-%d')
            dicts.append({'date': name, 'forecast_dict': d})
        writer.writeheader()
        writer.writerows(dicts)

    return check_weather(forecast_file, date)


if delta_time.days > days_amount - 1 or delta_time.days < 0:
    print('Nie wiadomo czy będzie padać - data spoza {}-dniowego zakresu prognozy pogody.'.format(days_amount))
else:
    ans = check_weather(forecast_file, date)
    if not ans:
        print('------Import prognozy pogody------')
        response = import_current_forecast(city, days_amount, key)
        if response.status_code != 200:
            print('Błąd importu prognozy pogody.')
            sys.exit()
        else:
            forecast_list = response.json()['list']
            ans = update_forecast_and_check(forecast_list, forecast_file, date)
    print('Prognoza na dzień {}:'.format(date))
    print(ans)

