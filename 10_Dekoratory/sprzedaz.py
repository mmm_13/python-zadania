"""
Program sprzedaz.py zwraca do pliku informację o wszystkich wykonanych akcjach,
pozwala na aktualizację magazynu poprzez sprzedaż produktów.
"""

from accountant import SaveActions

saver = SaveActions('results.txt')
saver.save_results()