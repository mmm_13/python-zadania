"""
Program saldo.py zwraca do pliku informację o wszystkich wykonanych akcjach,
pozwala na aktualizację wartości salda o wskazana wartość wraz z komentarzem opisujacym rodzaj wpłaty.
"""

from accountant import SaveActions

saver = SaveActions('results.txt')
saver.save_results()