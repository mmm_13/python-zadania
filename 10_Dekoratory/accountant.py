"""
Nowy ulepszony pakiet funkcji i klas do obsługi systemu ksiegowego.
Zastosowano reguły SOLID.
"""
import sys
import os
from pathlib import Path
import copy


class Product:

    def __init__(self, name):
        self.name = name
        self.amount = 0


class Manager:

    def __init__(self):
        self.callbacks = {}
        self.action_args = {}
        self.balance = 0
        self.temporary_actions = []
        self.actions = []
        self.products = {}

    def assign(self, action, count):
        def decorate(callback):
            self.callbacks[action] = callback
            self.action_args[action] = count

        return decorate

    def update_store(self):
        while self.temporary_actions:
            action = self.temporary_actions.pop(0)
            params = []
            if action in self.action_args:
                for i in range(self.action_args[action]):
                    params.append(self.temporary_actions.pop(0))
                self.actions.append((action, params))

    def execute(self):
        for action, params in self.actions:
            if action in self.callbacks:
                self.callbacks[action](params)


class ReadActions:

    def __init__(self, args_list):
        self.import_file = args_list[1]
        self.in_command = args_list[0].split(sep='.')[0]
        self.in_args = args_list[2:]
        self.input_actions = [self.in_command] + self.in_args
        self.actions_list = []

    def import_data_to_list(self):
        if os.path.isfile(self.import_file) and Path(self.import_file).suffix == '.txt':
            with open(self.import_file, 'r', encoding='utf8', newline='') as file:
                for line in file:
                    self.actions_list.append(line.strip())
            self.actions_list += self.input_actions
        else:
            print('Wskazana przez użytkownika ścieżka {} nie jest plikiem'
                  ' txt lub nie istnieje.'.format(self.import_file))
            sys.exit('Import danych niemożliwy.')


mg = Manager()
reader = ReadActions(sys.argv)
reader.import_data_to_list()


def zero_price_amount_test(price, amount):
    if price < 0:
        print('Błąd - cena produktu nie może być mniejsza od 0 gr.')
        print('Operacja została anulowana')
        return False
    elif amount < 0:
        print('Błąd - ilosć produktu do zakupu nie może być mniejsza od zera.')
        print('Operacja została anulowana')
        return False


def purchase_test(price, amount):
    zero_price_amount_test(price, amount)
    if price * amount > mg.balance:
        print('Produkt w podanej ilości sztuk nie może zostać zakupiony. Kwota zakupu przekracza wartość salda.')
        print('Operacja została anulowana')
        return False
    return True


def sell_test(name, price, amount):
    zero_price_amount_test(price, amount)
    if name not in mg.products:
        print("Produkt nie jest dostępny na magazynie")
        print('Operacja została anulowana')
        return False
    elif amount > mg.products[name].amount:
        print('Brak wystarczającej ilości produktów na magazynie')
        print('Operacja została anulowana')
        return False
    return True


@mg.assign('saldo', 2)
def saldo(params):
    quota = int(params[0])
    mg.balance += quota


@mg.assign('zakup', 3)
def buy_product(params):
    name = params[0]
    price = int(params[1])
    amount = int(params[2])
    test = purchase_test(price, amount)
    if not test:
        return False
    if name not in mg.products:
        product = Product(name)
        mg.products[name] = product
        mg.products[name].amount = amount
    else:
        mg.products[name].amount += amount
    mg.balance -= price * amount


@mg.assign('sprzedaz', 3)
def buy_product(params):
    name = params[0]
    price = int(params[1])
    amount = int(params[2])
    test = sell_test(name, price, amount)
    if not test:
        return False
    mg.products[name].amount -= amount
    mg.balance += price * amount


mg.temporary_actions = copy.copy(reader.actions_list)
mg.update_store()
mg.execute()


class SaveResults:

    def __init__(self, export_file):
        self.export_file = export_file
        self.results_list = []

    def save_results(self):
        self.make_results()
        with open(self.export_file, 'w') as file:
            for element in self.results_list:
                file.write('{}\n'.format(element))


class SaveActions(SaveResults):

    def make_results(self):
        self.results_list = copy.copy(reader.actions_list)


class SaveKonto(SaveResults):

    def make_results(self):
        self.results_list = [mg.balance]


class SaveMagazyn(SaveResults):

    def make_results(self):
        results = []
        for element in reader.in_args:
            if element in mg.products:
                info = '{} : {}'.format(element, mg.products[element].amount)
            else:
                info = '{} : {}'.format(element, 0)
            results.append(info)
        self.results_list = results


class SavePrzeglad(SaveResults):

    def make_results(self):
        results = []
        first_idx = int(reader.in_args[0])
        last_idx = int(reader.in_args[1])
        if first_idx > len(mg.actions):
            first_idx = len(mg.actions)
            last_idx = len(mg.actions)
        elif last_idx > len(mg.actions):
            last_idx = len(mg.actions)
        for i in range(first_idx, last_idx + 1):
            for x in mg.actions[i]:
                results.append(x)
        self.results_list = results

