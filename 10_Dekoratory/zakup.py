"""
Program zakup.py zwraca do pliku informację o wszystkich wykonanych akcjach,
pozwala na aktualizację magazynu poprzez zakup nowego produktu.
"""

from accountant import SaveActions

saver = SaveActions('results.txt')
saver.save_results()