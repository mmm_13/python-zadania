"""
Program do obsługi prostego systemu księgowego
"""

import sys

# zmienne
total_saldo = 0
actions = []
store = {}

while True:
    print("Wybierz jaką operację chcesz wykonać.")
    print("Dostępne operacje: 'saldo', 'zakup', 'sprzedaz', aby zakończyć wprowadzanie operacji wpisz 'stop'.")
    mode = input()
    if mode == 'saldo':
        saldo = int(input('Podaj wartość salda do wprowadzenia [gr]:  '))
        comment = input('Podaj komentarz do wprowadzonej zmiany wartości salda:  ')
        actions.append((mode, saldo, comment))
        total_saldo += saldo
        continue
    elif mode == 'zakup':
        product_name = input('Podaj nazwę produktu do zakupu na magazyn:  ')
        price = int(input('Podaj cene jednostkową produktu {} [gr]:  '.format(product_name)))
        if price < 0:
            print('Błąd - cena produktu nie moze być mniejsza od 0 gr')
            print('Operacja została anulowana')
            continue
        amount = int(input('Podaj ilość sztuk produktu {} do zakupienia:  '.format(product_name)))
        if amount < 0:
            print('Błąd - ilosć produktu do zakupu nie moze być mniejsza od zera.')
            print('Operacja została anulowana')
            continue
        total_price = price * amount
        if total_price > total_saldo:
            print('Produkt {} w ilości {} sztuk nie może zostać zakupiony.\nKwota zakupu przekracza wartość salda.')
            print('Operacja została anulowana')
            continue
        total_saldo -= total_price
        if product_name in store.keys():
            store[product_name] += amount
        else:
            store[product_name] = amount
        actions.append((mode, product_name, price, amount))
        continue
    elif mode == 'sprzedaz':
        product_name = input('Podaj nazwę produktu z magazynu do sprzedaży:  ')
        product_stock = store.get(product_name, "Produkt nie jest dostępny na magazynie")
        if type(product_stock) == str:
            print(product_stock)
            print('Operacja została anulowana')
            continue
        price = int(input('Podaj cene jednostkową sprzedaży produktu {} [gr]:  '.format(product_name)))
        if price < 0:
            print('Błąd - cena produktu nie moze być mniejsza od 0 gr.')
            print('Operacja została anulowana')
            continue
        amount = int(input('Podaj ilość sztuk produktu {} do sprzedaży:  '.format(product_name)))
        if amount > product_stock:
            print("""Operacja nie może zostać zrealizowana - 
                brak wystarczjącej ilości produktu {} na magazynie""".format(product_name))
            print('Operacja została anulowana')
            continue
        elif amount < 0:
            print('Błąd - ilosć produktu do zakupu nie moze być mniejsza od zera.')
            print('Operacja została anulowana')
            continue
        store[product_name] -= amount
        total_saldo += price * amount
        actions.append((mode, product_name, price, amount))
        continue
    elif mode == 'stop':
        actions.append((mode,))
        break
    else:
        print('Wprowadzono nieprawidłową wartość operacji.\nDziałanie programu zostaje zakończone.')
        break

init_args = sys.argv[1:]
mode = init_args[0]
first_option = ['saldo', 'sprzedaz', 'zakup']
if mode in first_option:
    if mode == 'saldo':
        saldo = int(init_args[1])
        comment = init_args[2]
        actions.append((mode, saldo, comment))
        total_saldo += saldo
    elif mode == 'zakup':
        product_name = init_args[1]
        price = int(init_args[2])
        amount = int(init_args[3])
        total_price = price * amount
        if price < 0:
            print('Błąd - cena produktu nie moze być mniejsza od 0 gr')
            print('Operacja została anulowana')
        elif amount < 0:
            print('Błąd - ilosć produktu do zakupu nie moze być mniejsza od zera.')
            print('Operacja została anulowana')
        elif total_price > total_saldo:
            print('Produkt {} w ilości {} sztuk nie może zostać zakupiony.\nKwota zakupu przekracza wartość salda.')
            print('Operacja została anulowana')
        else:
            total_saldo -= total_price
            if product_name in store.keys():
                store[product_name] += amount
            else:
                store[product_name] = amount
            actions.append((mode, product_name, price, amount))
    elif mode == 'sprzedaz':
        product_name = init_args[1]
        price = int(init_args[2])
        amount = int(init_args[3])
        product_stock = store.get(product_name, "Produkt nie jest dostępny na magazynie")
        if type(product_stock) == str:
            print(product_stock)
            print('Operacja została anulowana')
        elif price < 0:
            print('Błąd - cena produktu nie moze być mniejsza od 0 gr.')
            print('Operacja została anulowana')
        elif amount > product_stock:
            print("""Operacja nie może zostać zrealizowana - 
                        brak wystarczjącej ilości produktu {} na magazynie""".format(product_name))
            print('Operacja została anulowana')
        elif amount < 0:
            print('Błąd - ilosć produktu do zakupu nie moze być mniejsza od zera.')
            print('Operacja została anulowana')
        else:
            store[product_name] -= amount
            total_saldo += price * amount
            actions.append((mode, product_name, price, amount))
    print('Wszystkie parametry wprowadzone do systemu:')
    for x in actions:
        for y in x:
            print(y)
elif mode == 'konto':
    print('Po wszystkich operacjach saldo wynosi:\n{}'.format(total_saldo))
elif mode == 'magazyn':
    product_idx = init_args[1:]
    print('Podsumowanie stanów magazynowych:')
    for idx in product_idx:
        print('{} : {}'.format(idx, store.get(idx, 0)))
elif mode == 'przeglad':
    first_idx = int(init_args[1])
    last_idx = int(init_args[2])+1
    if first_idx > len(actions):  # ograniczenie indeksów do długości listy actions
        first_idx = len(actions)
        last_idx = len(actions)
    elif last_idx > len(actions):
        last_idx = len(actions)
    print('Podsumowanie wybranych akcji')
    for i in range(first_idx, last_idx):
        print('Akcja {}:'.format(i+1))
        for x in actions[i]:
            print(x)
