"""
Program wyliczający pozostałą wartość kwoty kredytu
"""

kredyt = int(input('Podaj wartość kredytu: '))
rata = int(input('Podaj wartość stałej raty kredytu: '))
procent = float(input('Podaj wartość oprocentowania kredytu: '))


# wartości inflacji w poszczególnych miesiącach spłaty kredytu
inf_1_1 = 1.59282448436825
inf_1_2 = -0.453509101198007
inf_1_3 = 2.32467171712441
inf_1_4 = 1.26125440724877
inf_1_5 = 1.78252628571251
inf_1_6 = 2.32938454145522
inf_1_7 = 1.50222984223283
inf_1_8 = 1.78252628571251
inf_1_9 = 2.32884899407637
inf_1_10 = 0.616921348207244
inf_1_11 = 2.35229588637833
inf_1_12 = 0.337779545187098
inf_2_1 = 1.57703524727525
inf_2_2 = -0.292781442607648
inf_2_3 = 2.48619659017508
inf_2_4 = 0.267110317834564
inf_2_5 = 1.41795267229799
inf_2_6 = 1.05424326726375
inf_2_7 = 1.4805201044812
inf_2_8 = 1.57703524727525
inf_2_9 = -0.0774206903147018
inf_2_10 = 1.16573339872354
inf_2_11 = -0.404186717638335
inf_2_12 = 1.49970852083123

# obliczenia
stala = 1200
start = kredyt
X = (1 + ((inf_1_1+procent)/stala))*start - rata
Y = start - X
print('*'*7,'PIERWSZY ROK','*'*7)
print('Styczeń 1:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_1_2+procent)/stala))*start - rata
Y = start - X
print('Luty 1:\t\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_1_3+procent)/stala))*start - rata
Y = start - X
print('Marzec 1:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_1_4+procent)/stala))*start - rata
Y = start - X
print('Kwiecień 1:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_1_5+procent)/stala))*start - rata
Y = start - X
print('Maj 1:\t\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_1_6+procent)/stala))*start - rata
Y = start - X
print('Czerwiec 1:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_1_7+procent)/stala))*start - rata
Y = start - X
print('Lipiec 1:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_1_8+procent)/stala))*start - rata
Y = start - X
print('Sierpień 1:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_1_9+procent)/stala))*start - rata
Y = start - X
print('Wrzesień 1:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_1_10+procent)/stala))*start - rata
Y = start - X
print('Październik 1:\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_1_11+procent)/stala))*start - rata
Y = start - X
print('Listopad 1:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_1_12+procent)/stala))*start - rata
Y = start - X
print('Grudzień 1:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))


print('*'*7,'DRUGI ROK','*'*7)
start = X
X = (1 + ((inf_2_1+procent)/stala))*start - rata
Y = start - X
print('Styczeń 2:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_2_2+procent)/stala))*start - rata
Y = start - X
print('Luty 2:\t\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_2_3+procent)/stala))*start - rata
Y = start - X
print('Marzec 2:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_2_4+procent)/stala))*start - rata
Y = start - X
print('Kwiecień 2:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_2_5+procent)/stala))*start - rata
Y = start - X
print('Maj 2:\t\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_2_6+procent)/stala))*start - rata
Y = start - X
print('Czerwiec 2:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_2_7+procent)/stala))*start - rata
Y = start - X
print('Lipiec 2:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_2_8+procent)/stala))*start - rata
Y = start - X
print('Sierpień 2:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_2_9+procent)/stala))*start - rata
Y = start - X
print('Wrzesień 2:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_2_10+procent)/stala))*start - rata
Y = start - X
print('Październik 2:\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_2_11+procent)/stala))*start - rata
Y = start - X
print('Listopad 2:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))
start = X
X = (1 + ((inf_2_12+procent)/stala))*start - rata
Y = start - X
print('Grudzień 2:\t\tTwoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu.'.format(X,Y))