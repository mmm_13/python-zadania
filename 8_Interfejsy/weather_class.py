"""
Zdefiniowanie klasy WeatherForecast do wykorzystania w programie weather2.py.
"""

import requests
import csv
import json
import os
import datetime
from datetime import datetime as dt

class WeatherForecast:


    def __init__(self, key, city):
        self.key = key
        self.city = city
        self.day_amount = 16
        self.forecast_file = 'forecast_{}.csv'.format(city.split(',')[0])
        self.forecast = {}
        self.today = dt.combine(datetime.date.today(), dt.min.time())

    def import_current_forecast(self):
        url = "https://community-open-weather-map.p.rapidapi.com/forecast/daily"
        querystring = {"q": self.city, "cnt": self.day_amount, "units": "metric or imperial", "lang": "en"}
        headers = {'x-rapidapi-key': self.key,
                   'x-rapidapi-host': "community-open-weather-map.p.rapidapi.com"}

        return requests.request("GET", url, headers=headers, params=querystring)

    def weather_interpreter(self, ans_dict):
        if ans_dict['weather'][0]['main'] == 'Rain':
            return 'Będzie padać.'
        return 'Nie będzie padać.'

    def check_weather(self, date):
        if not os.path.exists(self.forecast_file):
            return False
        if not self.forecast:
            with open(self.forecast_file, 'r', newline='') as file:
                reader = csv.reader(file)
                next(reader)
                for row in reader:
                    name, forecast_str = row[0], row[1]
                    forecast_str = forecast_str.replace("\'", "\"")
                    day_forecast = json.loads(forecast_str)
                    self.forecast[name] = self.weather_interpreter(day_forecast)
        return self.forecast[date]

    def update_forecast_and_check(self, forecast_list, date):
        with open(self.forecast_file, 'w', newline='') as file:
            fields = ['date', 'forecast_dict']
            dicts = []
            writer = csv.DictWriter(file, fields)
            for d in forecast_list:
                name = dt.combine(dt.utcfromtimestamp(d['dt']), dt.min.time()).strftime('%Y-%m-%d')
                dicts.append({'date': name, 'forecast_dict': d})
            writer.writeheader()
            writer.writerows(dicts)
        return self.check_weather(date)

    def forecast_results(self, date):
        delta_time = dt.strptime(date, '%Y-%m-%d') - self.today
        if delta_time.days > self.day_amount - 1 or delta_time.days < 0:
            return 'Nie wiadomo czy będzie padać - data spoza {}-dniowego zakresu prognozy pogody.'.format(
                self.day_amount)
        ans = self.check_weather(date)
        if not ans:
            print('------Import prognozy pogody------')
            response = self.import_current_forecast()
            if response.status_code != 200:
                return 'Błąd importu prognozy pogody.'
            forecast_list = response.json()['list']
            ans = self.update_forecast_and_check(forecast_list, date)
        return ans

    def __getitem__(self, date):
        return self.forecast_results(date)

    def __str__(self):
        return 'Prognoza pogody {}'.format(self.city)

    def items(self):
        for i in self.forecast.items():
            yield i

    def __iter__(self):
        return iter(self.forecast)
