"""
Program stanowi usprawnioną wersję programu weather.py do zwracania informacji o pogodzie (opadach) w danym dniu
wskazanym przez użytkownika lub w dniu następnym (przy braku wskazania daty). Program został usprawniony poprzez
zastosowanie klasy WeatherForecast, korzystającej z magicznych metod.
Dane pogodowe są pobierane z wykorzystaniem zewnętrznego API:
 https://rapidapi.com/community/api/open-weather-map/
"""
import sys
import datetime
from datetime import datetime as dt
from weather_class import WeatherForecast

if len(sys.argv) < 2:
    print('Nie podano wystarczającej ilości argumentów wejściowych. Brak klucza API.')
    sys.exit('Działanie programu zostanie przerwane')
elif len(sys.argv) == 2:
    date = datetime.timedelta(days=1) + dt.combine(datetime.date.today(), dt.min.time())
    date = date.strftime('%Y-%m-%d')
else:
    date_str = sys.argv[2]
    date = dt.strptime(date_str, '%Y-%m-%d').strftime('%Y-%m-%d')

key = sys.argv[1]
city = "Warsaw,PL"
city2 = "Berlin,DE"

wf = WeatherForecast(key, city)
wf2 = WeatherForecast(key, city2)

print(date)
for w in [wf, wf2]:
    print(w, '\n{}'.format(w[date]))

print('-'*30)

print('Podsumowanie prognozy pogody na 16 dni:')
for w in wf.items():
    print(w)

print('-'*30)

print('Daty, dla których można sprawdzić prognozę pogody:')
for w in [wf, wf2]:
    print(w.city)
    for date in w:
        print(date)

