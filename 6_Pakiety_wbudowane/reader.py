"""
Program reader.py służy do modyfikacji wybranych danych w plikach csv.
"""

import sys
import csv
import os

if len(sys.argv) >= 3:
    import_path = sys.argv[1]
    export_path = sys.argv[2]
    changes = sys.argv[3:]
else:
    print('Podano nieodpowiednią liczbę argumentów wejściowych.')
    sys.exit()

rows = []
cwd = os.getcwd()

#  Sprawdzenie czy ścieżka istnieje oraz czy jest plikiem.
if not os.path.exists(import_path) or not os.path.isfile(import_path):
    print('Wskazana przez użytkownika ścieżka {} nie istnieje lub nie jest plikiem'.format(import_path))
    print('W bieżącym katalogu znajdują się następujące pliki:')
    file_list = os.listdir(cwd)
    for file in file_list:
        print(file)
    exit()

#  Otwarcie pliku csv w trybie odczytu
with open(import_path, 'r', encoding='utf8', newline='') as csv_file:
    reader = csv.reader(csv_file)
    for row in reader:
        rows.append(row)

#  Wprowadzenie modyfikacji w pliku
for change in changes:
    x, y, value = change.split(sep=',', maxsplit=2)
    x = int(x)
    y = int(y)
    if x > len(rows)-1:
        print('Numer wiersza przekracza zakres pliku csv - zmiana nie może zostać wprowadzona')
        continue
    else:
        if y > len(rows[x])-1:
            print('Numer kolumny przekracza zakres pliku csv - zmiana nie może zostać wprowadzona')
        else:
            if x == 0:
                print('Uwaga zmodyfikowano nazwę kolumny')
            rows[x][y] = value

print('Wprowadzono zmiany w pliku w pliku csv:')

#  Zapisanie modyfikacji do nowej lokalizacji oraz wyświetlenie w terminalu
results = sys.stdout
with open(os.path.join(cwd, export_path), 'w', newline='', encoding='utf8') as file:
    writer1 = csv.writer(results)
    writer2 = csv.writer(file)
    writer1.writerows(rows)
    writer2.writerows(rows)




