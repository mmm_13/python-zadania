"""
Skrypt służy do opracowaniu przykładowego pliku csv z danymi do wykonania zadania.
"""

import csv
file = 'example_people_data.csv'
fieldnames = ['first_name', 'last_name', 'age', 'city', 'profession']
source_data = [{'first_name': 'Anna', 'last_name': 'Nowak', 'age': 27, 'city': 'Warsaw', 'profession': 'teacher'},
               {'first_name': 'Maciej', 'last_name': 'Kowalski', 'age': 59, 'city': 'Berlin', 'profession': 'baker'},
               {'first_name': 'Laura', 'last_name': 'Kwiatkowska', 'age': 19, 'city': 'London', 'profession': 'student'},
               {'first_name': 'Mateusz', 'last_name': 'Leśny', 'age': 36, 'city': 'Warsaw', 'profession': 'programmer'},
               {'first_name': 'Krzysztof', 'last_name': 'Adamski', 'age': 29, 'city': 'Paris', 'profession': 'artist'},
               {'first_name': 'Matylda', 'last_name': 'Kot', 'age': 49, 'city': 'Madrid', 'profession': 'doctor'}]

with open(file, 'w', newline='', encoding='utf8') as csv_file:
    writer = csv.DictWriter(csv_file, fieldnames)

    writer.writeheader()
    writer.writerows(source_data)