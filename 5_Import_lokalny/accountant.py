"""
Pakiet funkcji i klas do opracowania systemu księgowego.
"""


class Product:

    def __init__(self, name):
        self.name = name
        self.amount = 0


class Store:

    def __init__(self, import_path, export_path):
        self.import_path = import_path
        self.export_path = export_path
        self.products = {}
        self.balance = 0
        self.actions = []
        self.temporary_actions = []

    def make_input_list(self):
        with open(self.import_path) as file:
            for line in file:
                self.temporary_actions.append(line.strip())

    def zero_price_amount_test(self, price, amount):
        if price < 0:
            print('Błąd - cena produktu nie może być mniejsza od 0 gr.')
            print('Operacja została anulowana')
            return False
        elif amount < 0:
            print('Błąd - ilosć produktu do zakupu nie może być mniejsza od zera.')
            print('Operacja została anulowana')
            return False

    def purchase_test(self, price, amount):
        self.zero_price_amount_test(price, amount)
        if price * amount > self.balance:
            print('Produkt w podanej ilości sztuk nie może zostać zakupiony. Kwota zakupu przekracza wartość salda.')
            print('Operacja została anulowana')
            return False
        return True

    def sell_test(self, name, price, amount):
        self.zero_price_amount_test(price, amount)
        if name not in self.products:
            print("Produkt nie jest dostępny na magazynie")
            print('Operacja została anulowana')
            return False
        elif amount > self.products[name].amount:
            print('Brak wystarczającej ilości produktów na magazynie')
            print('Operacja została anulowana')
            return False
        return True

    def update_balance(self):
        quota = int(self.temporary_actions.pop(0))
        comment = self.temporary_actions.pop(0)
        self.balance += quota
        self.actions.append(('saldo', quota, comment))

    def sell_buy_step(self):
        name = self.temporary_actions.pop(0)
        price = int(self.temporary_actions.pop(0))
        amount = int(self.temporary_actions.pop(0))

        return name, price, amount

    def buy_product(self):
        name, price, amount = self.sell_buy_step()
        purchase_test = self.purchase_test(price, amount)
        if not purchase_test:
            return False
        if name not in self.products:
            product = Product(name)
            self.products[name] = product
            self.products[name].amount = amount
        else:
            self.products[name].amount += amount
        self.balance -= price * amount
        self.actions.append(('zakup', name, price, amount))

    def sell_product(self):
        name, price, amount = self.sell_buy_step()
        sell_test = self.sell_test(name, price, amount)
        if not sell_test:
            return False
        self.products[name].amount -= amount
        self.balance += price * amount
        self.actions.append(('sprzedaz', name, price, amount))

    def update_store(self):
        while self.temporary_actions:
            current_mode = self.temporary_actions.pop(0)
            if current_mode == 'saldo':
                self.update_balance()
                continue
            elif current_mode == 'zakup':
                self.buy_product()
                continue
            elif current_mode == 'sprzedaz':
                self.sell_product()
                continue
            elif current_mode == 'stop':
                self.actions.append(('stop',))
                break
            else:
                print('Wprowadzono nieprawidłową wartość operacji.\nDziałanie programu zostaje zakończone.')
                break

    def save_results(self, results_list):
        with open(self.export_path, 'w') as file:
            for element in results_list:
                file.write('{}\n'.format(element))

    def display_actions(self):
        results = []
        for x in self.actions:
            for y in x:
                results.append(y)
        self.save_results(results)

    def display_saldo(self, init_args_list):
        self.temporary_actions = init_args_list
        self.update_balance()
        self.display_actions()

    def display_zakup(self, init_args_list):
        self.temporary_actions = init_args_list
        self.buy_product()
        self.display_actions()

    def display_sprzedaz(self, init_args_list):
        self.temporary_actions = init_args_list
        self.sell_product()
        self.display_actions()

    def display_konto(self):
        self.actions.append(('konto',))
        self.save_results([self.balance])

    def display_magazyn(self, init_args_list):
        results = []
        new_actions = ['magazyn']
        for element in init_args_list:
            if element in self.products:
                info = '{} : {}'.format(element, self.products[element].amount)
            else:
                info = '{} : {}'.format(element, 0)
            results.append(info)
            new_actions.append(element)
        self.actions.append(tuple(new_actions))
        self.save_results(results)

    def display_przeglad(self, init_args_list):
        results = []
        first_idx = int(init_args_list[0])
        last_idx = int(init_args_list[1])
        self.actions.append(('przeglad', first_idx, last_idx))
        if first_idx > len(self.actions):  # ograniczenie indeksów do długości listy actions
            first_idx = len(self.actions)
            last_idx = len(self.actions)
        elif last_idx > len(self.actions):
            last_idx = len(self.actions)
        for i in range(first_idx, last_idx+1):
            for x in self.actions[i]:
                results.append(x)
        self.save_results(results)
