"""
Program magazyn.py zwraca do pliku informację stanach magazynowych dla skazanych produktów
"""

import sys
from accountant import Store

if len(sys.argv) >= 3:

    import_path = sys.argv[1]
    init_args = sys.argv[2:]
    export_path = 'results.txt'

    store = Store(import_path, export_path)
    print('Utworzono nową instancję klasy Store.')

    store.make_input_list()
    store.update_store()
    print('Zaimportowano i przetworzono dane z pliku teskstowego {}.'.format(import_path))

    store.display_magazyn(init_args)
    print('Zapisano wynik działania programu {} do pliku {}.'.format(sys.argv[0], export_path))
else:
    print('Przekazano błędną ilość parametrów wejściowych')