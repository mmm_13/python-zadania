"""
Program sprzedaz.py zwraca do pliku informację o wszystkich wykonanych akcjach,
pozwala na aktualizację magazynu poprzez sprzedaż produktów.
"""

import sys
from accountant import Store

if len(sys.argv) == 5:

    import_path = sys.argv[1]
    init_args = sys.argv[2:]
    export_path = 'results.txt'

    store = Store(import_path, export_path)
    print('Utworzono nową instancję klasy Store.')

    store.make_input_list()
    store.update_store()
    print('Zaimportowano i przetworzono dane z pliku teskstowego {}.'.format(import_path))

    store.display_sprzedaz(init_args)
    print('Zapisano wynik działania programu {} do pliku {}.'.format(sys.argv[0], export_path))
else:
    print('Przekazano błędną ilość parametrów wejściowych')