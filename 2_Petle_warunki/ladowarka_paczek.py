"""
Program do obsługi ładowarki paczek
"""
import sys


# warunki
element_amount = int(sys.argv[1])
max_load_pack = 20
min_weight = 1
max_weight = 10

send_packs = 0
kg_bufor = 0
send_kg = 0
pack_weight = 0
empty_kg_max = 0
buf = 0
print('Maksymalna liczba elementów do wysłania = ', element_amount)


for element in range(element_amount):
    weight = float(input('Podaj wagę {} elementu '.format(element+1)))
    if weight > max_weight or weight < min_weight:
        if weight != 0:
            print('Błąd - podano nieprawidłową wagę paczki !')
        else:
            print('Zakończono proces pakowania')
        if element == 0:
            break
        else:
            send_packs += 1
            send_kg += kg_bufor
            buf = max_load_pack - kg_bufor
            if empty_kg_max < buf:
                empty_kg_max = buf
            break
    kg_bufor += weight
    if kg_bufor > max_load_pack and element < (element_amount-1):
        send_packs += 1
        send_kg += kg_bufor - weight
        buf = max_load_pack - kg_bufor - weight
        if empty_kg_max < buf:
            empty_kg_max = buf
        kg_bufor = weight
    elif kg_bufor > max_load_pack and element == (element_amount-1):
        print('Zakończono proces pakowania')
        send_packs += 2
        send_kg += kg_bufor
        buf = max_load_pack - weight
        if empty_kg_max < buf:
            empty_kg_max = buf
    elif kg_bufor < max_load_pack and element == (element_amount-1):
        print('Zakończono proces pakowania')
        send_packs += 1
        send_kg += kg_bufor
        buf = max_load_pack - kg_bufor
        if empty_kg_max < buf:
            empty_kg_max = buf


print('*'*10, 'PODSUMOWANIE', '*'*10)
print('Liczba wysłanych paczek = ', send_packs)
print('Liczba wysłanych kilogramów = ', send_kg)
print('Suma pustych kilogramów = ', send_packs*20-send_kg)
print('Największa liczba pustych kilogramów w paczce = ', empty_kg_max)
