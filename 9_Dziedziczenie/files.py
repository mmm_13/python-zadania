"""
Plik files.py definiuje klasy do obsługi(odczytu, modyfikacji, zapisu) róźnych rodzajów plików.
"""
import json
import pickle
import csv
import sys
import os


class ReadFile:

    def __init__(self, import_file):
        self.import_file = import_file
        self.data = []

    def read_data(self, character='r'):
        if not os.path.exists(self.import_file) or not os.path.isfile(self.import_file):
            print('Wskazana przez użytkownika ścieżka {} nie istnieje lub nie jest plikiem'.format(self.import_file))
            print('W bieżącym katalogu znajdują się następujące pliki:')
            file_list = os.listdir(os.getcwd())
            for file in file_list:
                print(file)
            exit()
        self.data = []
        try:
            with open(self.import_file, character, newline='', encoding='utf-8') as file:
                self.read_file(file)
        except ValueError:
            with open(self.import_file, character, buffering=30) as file:
                self.read_file(file)

    def change_data(self, changes):
        results = sys.stdout
        for change in changes:
            x, y, value = change.split(sep=',', maxsplit=2)
            x = int(x)
            y = int(y)
            if x > len(self.data) - 1:
                print('Numer wiersza przekracza zakres pliku - zmiana:\n{} - '
                      'nie może zostać wprowadzona.'.format(change))
            else:
                if y > len(self.data[x]) - 1:
                    print('Numer kolumny przekracza zakres pliku csv - zmiana:\n{} - '
                          'nie może zostać wprowadzona.'.format(change))
                else:
                    if x == 0:
                        print('Uwaga zmodyfikowano nazwę kolumny')
                    self.data[x][y] = value
        results.write(str(self.data))


class ReadCSV(ReadFile):

    def read_file(self, file):
        reader = csv.reader(file)
        for row in reader:
            self.data.append(row)


class ReadJson(ReadFile):

    def read_file(self, file):
        self.data = json.loads(file.read())


class ReadPickle(ReadFile):

    def read_file(self, file):
        self.data = pickle.load(file)

    def read_data(self, character='rb'):
        super().read_data(character)


class SaveFile:

    def __init__(self, export_file):
        self.export_file = export_file

    def save_changes(self, character='w'):
        try:
            with open(os.path.join(self.export_file), character, newline='', encoding='utf_8') as file:
                self.save_to_file(file)
        except ValueError:
            with open(os.path.join(self.export_file), character) as file:
                self.save_to_file(file)


class SaveCSV(SaveFile):

    def save_to_file(self, file):
        writer = csv.writer(file)
        writer.writerows(self.data)


class SaveJson(SaveFile):

    def save_to_file(self, file):
        json.dump(self.data, file)


class SavePickle(SaveFile):

    def save_to_file(self, file):
        pickle.dump(self.data, file)

    def save_changes(self, character='wb'):
        super().save_changes(character)


read_dict = {'.csv': ReadCSV, '.json': ReadJson, '.pickle': ReadPickle}
save_dict = {'.csv': SaveCSV, '.json': SaveJson, '.pickle': SavePickle}


def choose_class(source, dest):
    source_class = None
    dest_class = None
    source_class = read_dict[source]
    dest_class = save_dict[dest]

    class Ret(source_class, dest_class):

        def __init__(self, import_file, export_file):
            source_class.__init__(self, import_file)
            dest_class.__init__(self, export_file)

    return Ret
