"""
Program stanowi usprawnioną wersje reader.py do modyfikacji wybranych danych w plikach csv.
Nowa wersja pozwala na modyfikację dowolnych plików z rozszerzeniem *.csv, *.json, *.pickle.
"""

import sys
import os
from pathlib import Path
from files import choose_class
cwd = os.getcwd()

try:
    import_path = sys.argv[1]
    export_path = sys.argv[2]
    try:
        changes = sys.argv[3:]
    except IndexError:
        changes = []
except IndexError:
    print('Podano nieodpowiednią liczbę argumentów wejściowych.')
    sys.exit()

source = Path(import_path).suffix
dest = Path(export_path).suffix


file_class = choose_class(source, dest)
file_changer = file_class(import_path, export_path)
file_changer.read_data()
file_changer.change_data(changes)
file_changer.save_changes()



